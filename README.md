# xivo-python-flask-restful-packaging

Debian packaging for [flask-restful](https://flask-restful.readthedocs.org) used in XiVO.

## Upgrading

To upgrade flask-restful:

* Update the version number in the `VERSION` file
* Update the changelog using `dch -i` to the matching version
* Push the changes
